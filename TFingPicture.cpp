#include "TFingPicture.h"

///////////////////////////////////////////////////////////////////////////////
//����� �����������.
//�������� �����������, ���������� ���������� �������� ��� ���
///////////////////////////////////////////////////////////////////////////////
//���������� ���������� �����
const CPoint incXY[8]=
{
	CPoint(-1, -1),
	CPoint(0,  -1),
	CPoint(1,  -1),
	CPoint(1,  0),
	CPoint(1,  1),
	CPoint(0,  1),
	CPoint(-1, 1),
	CPoint(-1, 0)};
TFingPicture::TFingPicture(CDC *_Screen)
{
	Screen = _Screen;
	pic.CreateCompatibleDC(Screen);
	IsLoad = false;
}
TFingPicture::~TFingPicture(void){}
//���������� ����������� �� ���� � ����������� (X,Y)
bool TFingPicture::Show(int X, int Y)
{
	if (!IsLoad) return false;
	int kx = bmp.bmWidth;
	int ky = bmp.bmHeight;
	return Screen->StretchBlt(X, Y, bmp.bmWidth, bmp.bmHeight, &pic, 0, 0,  kx, ky, SRCCOPY)>0;
}
//��������� ����������� �� ����� src
bool TFingPicture::Load(const CString src)
{	
	IsLoad = false;
	CBitmap bm;
	bm.Detach();
	IsLoad = bm.Attach(LoadImage(0, src, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE))>0;
	bm.GetObject(sizeof(BITMAP), &bmp);
	pic.SelectObject(&bm);
	return IsLoad;
}
// color = BGR;
bool TFingPicture::SetPixel(CPoint dot, COLORREF color)
{
	if (!IsLoad) return false;
	pic.SetPixel(dot.x, dot.y, color);
	return true;
}
bool TFingPicture::SetPixel(int x, int y, COLORREF color)
{
	if (!IsLoad) return false;
	pic.SetPixel(x, y, color);
	return true;
}
// color = BGR;
COLORREF TFingPicture::GetPixel(CPoint dot)
{
	if (!IsLoad) return false;
	return pic.GetPixel(dot.x, dot.y);
}
COLORREF TFingPicture::GetPixel(int x, int y)
{
	if (!IsLoad) return false;
	return pic.GetPixel(x, y);
}
bool TFingPicture::FloodFill(CPoint dot, COLORREF color)
{
	if(!IsLoad) return false;
	COLORREF col = GetPixel(dot);
	CBrush br(color);
	pic.SelectObject(&br);
	pic.ExtFloodFill(dot.x, dot.y, col, FLOODFILLSURFACE);
	return true;
}
bool TFingPicture::FloodFill(int x, int y, COLORREF color)
{
	if(!IsLoad) return false;
	COLORREF col = GetPixel(x, y);
	CBrush br(color);
	pic.SelectObject(&br);
	pic.ExtFloodFill(x, y, col, FLOODFILLSURFACE);
	return true;
}
bool TFingPicture::Line(CPoint from, CPoint to, int width, COLORREF color)
{
	if(!IsLoad) return false;
	CPen pen(PS_SOLID, width, color);
	pic.SelectObject(&pen);
	pic.MoveTo(from.x, from.y);
	pic.LineTo(to.x, to.y);
	return true;
}
bool TFingPicture::Rectangle(CPoint from, CPoint to, int width, COLORREF color)
{
	if(!IsLoad) return false;
	Line(from, CPoint(from.x, to.y), width, color);
	Line(CPoint(from.x, to.y), to, width, color);
	Line(to, CPoint(to.x, from.y), width, color);
	Line(CPoint(to.x, from.y), from, width, color);
	return true;
}
bool TFingPicture::Copy(TFingPicture &from)
{
	bmp = from.bmp;
	IsLoad = from.IsLoad;
	Screen = from.Screen;
	return pic.BitBlt(0, 0, bmp.bmWidth, bmp.bmHeight, &from.pic, 0, 0, SRCCOPY)>0;
}
CPoint TFingPicture::NextDotCW(const CPoint dot, int &vec)
//����� ��������� ����� "_�� �������_ �������"
//vec ��������� ����������� ������
{
	int i = vec, 
		step = 0;
	CPoint newdot = dot;
	COLORREF clMas[9];
	clMas[8] = clMas[0] = GetPixel(dot.x-1, dot.y-1);
	clMas[1] = GetPixel(dot.x,   dot.y-1);
	clMas[2] = GetPixel(dot.x+1, dot.y-1);
	clMas[3] = GetPixel(dot.x+1, dot.y);
	clMas[4] = GetPixel(dot.x+1, dot.y+1);
	clMas[5] = GetPixel(dot.x,   dot.y+1);
	clMas[6] = GetPixel(dot.x-1, dot.y+1);
	clMas[7] = GetPixel(dot.x-1, dot.y);
	do{
		if(clMas[i+1] < clMas[i])
		{
			vec = (i + 1) % 8;
			newdot.x = dot.x + incXY[vec].x;
			newdot.y = dot.y + incXY[vec].y;
			if(vec % 2 == 0) SetPixel(dot.x + incXY[vec+1].x, dot.y + incXY[vec+1].y, 0x000000);
			vec = (vec + 5) % 8;
			return newdot;	//������� ����� �����
		}
		i = (i + 1) % 8;
		step++;
	}while(step <= 8);
	return dot;	//����� �� � ���� �� ������
}
CPoint TFingPicture::NextDotCCW(const CPoint dot, int &vec)
//����� ��������� ����� "_������ �������_ �������"
//vec ��������� ����������� ������
{
	int i = vec, 
		step = 0;
	CPoint newdot = dot;
	COLORREF clMas[9];
	clMas[8] = clMas[0] = GetPixel(dot.x-1, dot.y-1);
	clMas[1] = GetPixel(dot.x-1, dot.y);
	clMas[2] = GetPixel(dot.x-1, dot.y+1);
	clMas[3] = GetPixel(dot.x,   dot.y+1);
	clMas[4] = GetPixel(dot.x+1, dot.y+1);
	clMas[5] = GetPixel(dot.x+1, dot.y);
	clMas[6] = GetPixel(dot.x+1, dot.y-1);
	clMas[7] = GetPixel(dot.x,   dot.y-1);

	do{
		if(clMas[i+1] < clMas[i])
		{
			vec = (i + 1) % 8;
			newdot.x = dot.x + incXY[(8-vec)%8].x;
			newdot.y = dot.y + incXY[(8-vec)%8].y;
			if(vec % 2 == 0) SetPixel(dot.x + incXY[8-vec-1].x, dot.y + incXY[8-vec-1].y, 0x000000);
			vec = (vec + 5) % 8;
			return newdot;	//������� ����� �����
		}
		i = (i + 1) % 8;
		step++;
	}while(step <= 8);
	return dot;	//����� �� � ���� �� ������
}
CPoint TFingPicture::GetSize()
//��������� ������� �����������
{	if(!IsLoad) return false;
	return CPoint(bmp.bmWidth, bmp.bmHeight);
}
