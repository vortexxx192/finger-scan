#pragma once
#include <afxwin.h>
#include <list>
#include <ctime>
#include <time.h>
#include <atltime.h>
#define _USE_MATH_DEFINES
#include <math.h>

#define _CRT_SECURE_NO_DEPRECATE

using namespace std;

//������� "����� �����"
//"����� �����" - ������ ����� ��� ���������
class TMapElDot{
public:
	CPoint coord;		//���������� �����
	bool pr1, pr2;		//�������� �����
public:
	TMapElDot(CPoint dot){pr1 = true; pr2 = true; coord = dot;};
	TMapElDot(){pr1 = true; pr2 = true;};
	~TMapElDot(){};
};
//"����� �����" - ������ ����� ��� ���������
class TMapDot{
public:
	list<TMapElDot> map;	//����� ����� �� �����������
	TMapDot(){};
	~TMapDot(){map.clear();};
};

//��������������� ����������
class TInfo{
public:
	short kol;			//���������� �����
	short dpi;			//�������� ��������� ��������� (dot per inch)
	CString src;			//���� � ������ �� �������� ���� �������� ����������
	CTime date;				//���� ���������
	CString description;	//��������
	bool operator==(const TInfo &inf){return src == inf.src;};	//��������� ������������ ����������� �� �����
	TInfo(){kol = -1; dpi = -1; /*src = ""; description = "";*/};
    void Printf(FILE *fout)		//������ ������ � ����
	{
		fwrite((void *)(&kol), sizeof(kol), 1, fout);
		fwrite((void *)(&dpi), sizeof(dpi), 1, fout);
		int strlen = src.GetLength();
		fwrite((void *)(&strlen), sizeof(int), 1, fout);
		fwrite((void *)(src.GetBuffer()), strlen, 1, fout);
	};
    void Scanf(FILE *fin)		//������ ������ �� �����
	{
		fread((void *)(&kol), sizeof(kol), 1, fin);
		fread((void *)(&dpi), sizeof(dpi), 1, fin);
		int strlen;
		fread((void *)(&strlen), sizeof(int), 1, fin);
		char * text = new char[strlen+1];
		fread((void *)(text), strlen, 1, fin);
		text[strlen] = '\0';
		src = text;
		delete(text);
	};
};

//���������� ��������� �����
class TAbsDot{
public:
	CPoint coord;	//����������
	double alpha;	//����������� � �����
	bool type;		//��� ����� (1- ���������, 0- ����������)
	bool show;		//��������� ����� (1- ������, 0- ������)
public:
	TAbsDot(){coord.x = -1; coord.y = -1; alpha = 0; type = false; show = false;};
	~TAbsDot(){};
	bool operator==(const TAbsDot &f){return (coord.x == f.coord.x && coord.y == f.coord.y && alpha == f.alpha);};
	bool operator <(const TAbsDot &f){return (alpha < f.alpha);};
	bool operator >(const TAbsDot &f){return (alpha > f.alpha);};
	bool operator!=(const TAbsDot &f){return false;};
	bool operator<=(const TAbsDot &f){return false;};
	bool operator>=(const TAbsDot &f){return false;};
	CString toStr()
	{	
		CString str;
		str.Format("%d %d %f %d %d\n", coord.x, coord.y, alpha, type, show);
		return str;
	};
};

//����� ��� �������� ����� � _����������_ ����������
//�������� ��������� � ���������� ����������
class TAbsFing: public list<TAbsDot>
{
public:
	TAbsFing(){this->clear();};
	~TAbsFing(){this->clear();};
	bool LoadFing(CString src);		//�������� ��������� �� ����� *.sav
    bool SaveFing(CString fsav);		//���������� ��������� � ���� *.sav
};

//������������� ��������� �����
class TRelDot{
public:
	short l,a1,a2;		//���������� �����
	//l - ��������� ����� �������
	//a1 - ���� ����� ����������� ������������ ����� � � ������������ A -> B	[0, 2*M_PI)
	//a2 - ���� ����� ����������� ������������ ����� � � ������������ A -> B	[0, 2*M_PI)
	TAbsDot absDot;	//�� ���������� ��������� (���������� ��� ����������� �� ������ ��������� �����)
public:
	bool operator<(const TRelDot &f){return this->l < f.l;}
	bool sortByA1(TRelDot &f){return a1 < f.a1;}			//��� ������� ����� ��� ����������, �� ���������� ��� � �� ������������
	bool operator==(const TRelDot &f){return (this->l == f.l && this->a1 == f.a1 && this->a2 == f.a2);}
	CString toStr(){CString s; s.Format("%d %d %d\n", l, a1, a2); return s;}
};

//����� ��� �������� _�������������_ ���������� �����
typedef list<TRelDot> listTRelDot;

//������ ��� �������� ���� �������� {first, second}
template <class data_t1, class data_t2> struct TPair{
	data_t1 first;
	data_t2 second;
	TPair(data_t1 _f, data_t2 _s){first = _f; second = _s;};
};
typedef TPair<TAbsDot, TAbsDot> TPairAbsDot;
typedef TPair<listTRelDot*, listTRelDot*> TPairSur;

//��������� ��������� ����������
struct TCompareFing{
	double val;			//������� �������� ����������
	short cDot;			//���������� ��������� �����
	short nfng;			//����� ���������
	CString name;		//���� ���������
	list<TPairAbsDot> dots;		//first - ��������� ����� �� ��������� � ����
								//second - ��������� ����� �� �������� ���������
	list<TPairSur> surdots;
	//��������� �� ���������� ���������� ������ ���� �����������,
	//�� ���� �������� ��������� "���"
};

//�������� ��������� � _�������������_ ����������
class TRelFing: public list<listTRelDot>{
private:
	inline double GetS(const CPoint A, const CPoint B);		//��������� ����� �������
	double GetAlpha(const CPoint A, const CPoint B);		//���������� �� ����� � � � [-pi,pi)
public:
	TRelFing(){};
	~TRelFing(){};
	TRelFing *Convert(TAbsFing &fng);		//�������������� ���������� ��������� � �������������
	TCompareFing Compare(TRelFing &fng);	//�������� ���������
};
