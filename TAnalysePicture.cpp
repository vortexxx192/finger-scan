#include "TAnalysePicture.h"

TAnalysePicture::TAnalysePicture(const CString src, CDC *screen)
{
	pic = new TFingPicture(screen);
	err = -1;
	if(!pic->Load(src)) err = 0;
	pic->Rectangle(CPoint(0, 0), pic->GetSize(), 10);
	srcImg = src;
	tmpPic = new TFingPicture(screen);
	tmpPic->Load(src);
	pic2 = new TFingPicture(screen);
	pic2->Load(BLANK);
}
TAnalysePicture::~TAnalysePicture(void)
{
	delete(tmpPic);
	delete(pic2);
	delete(pic);
}

//��� ������
int TAnalysePicture::getErr()
{
	return err;
}

//��������� ������
CString TAnalysePicture::getErrMsg()
{
	CString msg = "";
	switch (err)
	{
		case -1: {msg = "������ ��� �������� ����������� ���"; break;}
		case 0: {msg = "����������� �� ���������"; break;}
		case 1: {msg = "�������� ������ ��� �������� �����������"; break;}
		default: {msg = "�������������� ������";}
	}
	return msg;
}

// ��������� ������������ ����������� � ��������� ������
TAbsFing TAnalysePicture::AnalysePicture()
{
	TAbsFing ret, ret2;
	if(err != -1)
	{ 
		if(MESSAGEOUT) MessageBox(NULL, getErrMsg(), "������", MB_OK);
		return ret;
	}
	int prevCol;
	int changeN = 0;					//������� ������������� ��������� �� �����������
	list<TMapElDot> map;				//����� ����� ������������� ������
	list<TMapElDot>::iterator imap;		//�������� ��� map
	map = LookPic();					//������������ �������� � ���������� ����� �� ���
	do{
		changeN = 0;
		prevCol = (int)map.size();
		imap = map.begin();
		do{									//����������� ����� ��������������
			if(imap->pr1)		//����� ��������� � ���������
				changeN += ChangeLine(imap, map);	//��������� (��������������) �����������
			imap++;								//������� ��� ��������� ��������� �����
		}while(imap != map.end());				//����������� ����� ��������������
	}while(prevCol<0.1*map.size());				//����������� ����� ��������������

	map = LookPic();					//������������ �������� � ���������� ����� �� ���
	imap = map.begin();
	do{									//����������� ����� ��������������
		ret.merge(ReadPic(imap));
		imap++;							//������� ��� ��������� ��������� �����
	}while(imap != map.end());			//����������� ����� ��������������
////////////////////////////////////////////////////////////////////
/////////////////////������������ ���������� �����//////////////////
///����������� ������������� ������������ � ��������������� ������//
//////////� ��� �� ����� ����� � ������ �� ������� ��� �����////////
	int leftDots = 0;				//����� ��������� �����
	leftDots = DotsFilter(ret);		//������������ ���������� �����
////////////////////////////////////////////////////////////////////
	ret2.clear();
for(TAbsFing::iterator iter = ret.begin(); iter != ret.end(); iter++)
{	
	if(!iter->show) continue;

//��������� ��������� ����� (���� ��������� � ���������� ���������)
	COLORREF col = (iter->type)?0xFF0000:0x000000;
	pic2->Line(iter->coord, iter->coord, 5, col);
	pic2->Line(iter->coord, 
				CPoint(iter->coord.x+(int)(10.0*cos(iter->alpha)),iter->coord.y-(int)(10.0*sin(iter->alpha))),
				2, col);
	ret2.push_back(*iter);
}
ret.clear();
	return ret2;
}

TAbsFing TAnalysePicture::ReadPic(list<TMapElDot>::iterator _dot)
//���������� �� ����������� ���� �����
{
	TAbsFing retFing;		//����� ��������� � ���������� �����������
	int kol = 0;			//���������� ���������� �����
	int vec = 0;			//����������� ������ ��������� �����
	int tekS = 0;			//������� ���������� �������� ��������
	CPoint	A,				//������ �������
			B;				//����� �������

	TAbsFing vecDotS;		//������ ����� ��� �������� ��������
	TAbsFing vecDotL;		//������ ����� ��� ������� ��������
	TAbsFing historyDotL;		//������� ����� ��� ������� ��������
	TAbsDot _tmpDotFing, bestDot;
	TAbsFing::iterator iter;
	double alpha;				//����������� ������� (� ��������)
	int stopKol = 2000;			//������ �����
	int ret = 0;				//������� ����� ����� ����������� ��������� �����
	bool homeOver = false;		//������� ��������� ���������
	
	A = _dot->coord; B = _dot->coord;
	CPoint olddot, dot = _dot->coord;			//������� ����� �� �����
    
    do{
	//�������� ���� ���������, 
	//�������� ���������� �����
	//������������ �� ��� ��� ���� ��� ����� �� ����� �������� (���������� �������)
	//������������ (�� ���������� �������, �� ���������)
	//
		olddot = dot;
		dot = pic->NextDotCW(dot, vec);		//����� ��������� ����� _�� �������_ �������
		if(dot.x == olddot.x && dot.y == olddot.y)
		{//��������� ����� �� ���������� => �����//
			CString s;
			s.Format("x = %d, y = %d, kol= %d", dot.x, dot.y, kol);
			if(MESSAGEOUT)MessageBox(0, "��������� ����� �� ���������� => �����\n" + s, "", MB_OK);
			return retFing;
		}
		kol++;			//������� ���������� �����
		if(kol % LEN_S == 0)
		{//�������� ����� �������� ������
			tekS++;
			A = B;
			B = dot;
pic2->Line(A,B, 1, 0x999999);
			_tmpDotFing.coord = A;

			alpha = GetAlpha(A, B);		//������ ���������� ����������� ����� KOL_S ��������� (����������� ��������� �������)//
			double dAlpha = 0.0;		//������� �����
			if(vecDotS.size() > 0)		//� ������ ����� ����� ���������� ��������
				dAlpha = alpha - vecDotS.begin()->alpha;
/**/		if (abs(dAlpha) >= M_PI)	//������� ����� ����� ����� � ���������� �� ����������!
			{//���������� ��������������� ������� alpha
/**/			if (dAlpha < 0.0)
				{
					while (abs(dAlpha) > M_PI)
					{
						alpha += 2.0 * M_PI; 
						dAlpha += 2.0 * M_PI;
					}
				}else
				{	
					while (dAlpha >= M_PI)
					{
						alpha -= 2.0 * M_PI; 
						dAlpha -= 2.0 * M_PI;
					}	
				}
			}
			_tmpDotFing.alpha = alpha;		//����������� ����������� �� ����� �//
			vecDotS.push_front(_tmpDotFing);
///////////////////////////////////////////////////////////////////////
///////��������� ��� �������� ������� ������� ��� ������� ���//////////
///////�������� ���������� �����, ���� ���������� �������� �������/////
			if(vecDotS.size() < KOL_S) continue;
//���������� �������� ����������� LEN_L �������� ��������//
//������ ������ �� �������� �������////////////////////////
			double sumAlpha = 0.0;
			iter = vecDotS.begin();
			vecDotL.clear();	//����������� ������� �������
			for(int i = 0; i < KOL_S; i++)
			{
				sumAlpha += iter->alpha;
				if ((i+1) % LEN_L == 0)
				{
					_tmpDotFing = *iter;
					_tmpDotFing.alpha = sumAlpha / LEN_L;
					vecDotL.push_back(_tmpDotFing);
					sumAlpha = 0.0;
				}
				iter++;
			}
			if (abs(vecDotL.begin()->alpha) > 3*2*M_PI)
			{//������� ����� ��������//
				CString s;
				s.Format("alpha = %.2f", vecDotL.begin()->alpha*180);
				if(MESSAGEOUT)MessageBox(0, "������� ����� ��������\n"+s, "", MB_OK);
				return retFing;
			}
//��������� ��� �������� ������� �������//
			dAlpha = vecDotL.begin()->alpha - (++vecDotL.begin())->alpha;
			if (abs(dAlpha) > (TEST_ALPHA / 180.0 * M_PI))	//������� �����//
			{
				if (historyDotL.empty())
				{ //���������� ���������//
					bestDot.alpha = 0.0;
				}
				if (dAlpha > 0)		//����������
					alpha = (vecDotL.begin()->alpha - M_PI + (++vecDotL.begin())->alpha) / 2.0;
				else				//���������
					alpha = (vecDotL.begin()->alpha + M_PI + (++vecDotL.begin())->alpha) / 2.0;
				_tmpDotFing = vecDotL.front();
				_tmpDotFing.alpha = alpha;			//����������� � �� (����������� �����)//
				_tmpDotFing.type = dAlpha<0;		//��� ��//
				historyDotL.push_front(_tmpDotFing);
				if(bestDot.alpha <= abs(dAlpha)) 
				{	
					bestDot.coord = _tmpDotFing.coord;
					bestDot.alpha = abs(dAlpha);
				}
			}
			else //������� �����//
			{
				if (!historyDotL.empty())		//��� _�������_ ������� �����
				{
					alpha = 0;
					for(iter = historyDotL.begin(); iter != historyDotL.end(); iter++)
						alpha += iter->alpha;
					alpha /= historyDotL.size();		//������� �������� � ���������� ��
					iter = historyDotL.begin();
					for(unsigned int i = 0; i<(historyDotL.size()/2); i++) iter++;
//					CPoint wdot = iter->coord;			//�������� ��������� ����� ��� ��
					CPoint wdot = bestDot.coord;		//�������� ��������� ����� ��� ��
//���� ����������������� ��� ������, �� ���������� ����� ������� �����������
//					CPoint dotForAccept = FindAcceptDot(wdot, alpha, iter->type);
//					if (dotForAccept.x == -1)
					{	//����� �� ����� �����������, �������� ��//
						_tmpDotFing.alpha = ChangeAlphaInterval(alpha);
						_tmpDotFing.coord = wdot;
						_tmpDotFing.show = true;
						_tmpDotFing.type = historyDotL.begin()->type;
						retFing.push_back(_tmpDotFing);
					}
					historyDotL.clear();
					stopKol += (kol*1.5 > stopKol)?1000:0;
				}
			}
		}
		if (dot.x == _dot->coord.x && dot.y == _dot->coord.y)
		{//�������� ����� ����� ��������
			if (kol <= 2)
			{//����� ������������� ��������
				CString s;
				s.Format("%d", kol);
				if(MESSAGEOUT)MessageBox(0, "kol<=2   kol = " + s, "", MB_OK);
				return retFing;
			}else
			{
				homeOver = true;	//������ ���������� ������ ������	
				stopKol = kol + KOL_L*LEN_L*LEN_S;
			}
		}
		if (homeOver) ret++;
	}while(ret < (LEN_L*LEN_S*KOL_L) && ret < stopKol && kol <= stopKol);

	_dot->pr1 = false;
	_dot->pr2 = false;
    return retFing;
}
list<TMapElDot> TAnalysePicture::LookPic()
//������������ "����������" �� �������� � 
//����������� ������ �����, ����� ���������� ������ �����
//������� ���� ����� � ���� ���� (�������� ����� � ��������)
{
	list<TMapElDot> map;
	TMapElDot dot;

	tmpPic->Copy(*pic);
	
	for(int j = 0; j < pic->GetSize().y; j++)
		for(int i = 0; i < pic->GetSize().x; i++)
		{
			if(!tmpPic->GetPixel(i,j))		//������ ������ �������
			{
				dot.coord.x = i; dot.coord.y = j;
				dot.pr1 = dot.pr2 = true;
				map.push_back(dot);
				tmpPic->FloodFill(i, j, 0xffffff);	//�������� �����
			}
		}	
	tmpPic->Copy(*pic);
	return map;
}
int TAnalysePicture::ChangeLine(list<TMapElDot>::iterator _dot, list<TMapElDot> &_map)
//��������� ��������, �� ���������
//��������� ����� �� ������� ��������� imap
//����������� ������-���������� � ������-��������� �� ��������� �����
{
	int changeN = 0;				//���������� ����������� �� �����
	int kol = 0;			//���������� ���������� �����
	int vec = 0;			//����������� ������ ��������� �����
	int tekS = 0;			//������� ���������� �������� ��������
	CPoint	A,				//������ �������
			B;				//����� �������

	TAbsFing vecDotS;		//������ ����� ��� �������� ��������
	TAbsFing vecDotL;		//������ ����� ��� ������� ��������
	TAbsFing historyDotL;		//������� ����� ��� ������� ��������
	TAbsDot _tmpDotFing;
	TAbsFing::iterator iter;
	TAbsDot resetDot, bestDot;
	double alpha;				//����������� ������� (� ��������)
	int stopKol = 1500;			//������ �����
	int ret = 0;				//������� ����� ����� ����������� ��������� �����
	bool homeOver = false;		//������� ��������� ���������
	
	_dot->pr1 = false;
	A = _dot->coord; B = _dot->coord;
	CPoint olddot, dot = _dot->coord;			//������� ����� �� �����
    
    do{
	//�������� ���� ���������, 
	//�������� ���������� �����
	//������������ �� ��� ��� ���� ��� ����� �� ����� �������� (���������� �������)
	//������������ (�� ���������� �������, �� ���������)
	//
		olddot = dot;
		dot = pic->NextDotCW(dot, vec);		//����� ��������� ����� _�� �������_ �������
		if(dot.x == olddot.x && dot.y == olddot.y)
		{//��������� ����� �� ���������� => �����//
			CString s;
			s.Format("x = %d, y = %d, kol= %d", dot.x, dot.y, kol);
			if(MESSAGEOUT)MessageBox(0, "��������� ����� �� ���������� => �����\n" + s, "", MB_OK);
			return changeN;
		}
		kol++;			//������� ���������� �����
		if(kol % LEN_S == 0)
		{//�������� ����� �������� ������
			tekS++;
			A = B;
			B = dot;
//pic2->Line(A,B, 1, 0x999999);
			_tmpDotFing.coord = A;

			alpha = GetAlpha(A, B);		//������ ���������� ����������� ����� KOL_S ��������� (����������� ��������� �������)//
			double dAlpha = 0.0;		//������� �����
			if(vecDotS.size() > 0)		//� ������ ����� ����� ���������� ��������
				dAlpha = alpha - vecDotS.begin()->alpha;
/**/		if (abs(dAlpha) >= M_PI)	//������� ����� ����� ����� � ���������� �� ����������!
			{//���������� ��������������� ������� alpha
/**/			if (dAlpha < 0.0)
				{
					while (abs(dAlpha) > M_PI)
					{
						alpha += 2.0 * M_PI; 
						dAlpha += 2.0 * M_PI;
					}
				}else
				{	
					while (dAlpha >= M_PI)
					{
						alpha -= 2.0 * M_PI; 
						dAlpha -= 2.0 * M_PI;
					}	
				}
			}
			_tmpDotFing.alpha = alpha;		//����������� ����������� �� ����� �//
			vecDotS.push_front(_tmpDotFing);
///////////////////////////////////////////////////////////////////////
///////��������� ��� �������� ������� ������� ��� ������� ���//////////
///////�������� ���������� �����, ���� ���������� �������� �������/////
			if(vecDotS.size() < KOL_S) continue;
//���������� �������� ����������� LEN_L �������� ��������//
//������ ������ �� �������� �������////////////////////////
			double sumAlpha = 0.0;
			iter = vecDotS.begin();
			vecDotL.clear();	//����������� ������� �������
			for(int i = 0; i < KOL_S; i++)
			{
				sumAlpha += iter->alpha;
				if ((i+1) % LEN_L == 0)
				{
					_tmpDotFing = *iter;
					_tmpDotFing.alpha = sumAlpha / LEN_L;
					vecDotL.push_back(_tmpDotFing);
					sumAlpha = 0.0;
				}
				iter++;
			}
			if (abs(vecDotL.begin()->alpha) > 3*2*M_PI)
			{//������� ����� ��������//
				CString s;
				s.Format("alpha = %.2f", vecDotL.begin()->alpha*180);
				if(MESSAGEOUT)MessageBox(0, "������� ����� ��������\n"+s, "", MB_OK);
				return changeN;
			}
//��������� ��� �������� ������� �������//
			dAlpha = vecDotL.begin()->alpha - (++vecDotL.begin())->alpha;
			if (abs(dAlpha) > (TEST_ALPHA / 180.0 * M_PI))	//������� �����//
			{
				if (historyDotL.empty())
				{ //���������� ���������//
					resetDot = vecDotL.back();
					bestDot.alpha = 0.0;
				}
				if (dAlpha > 0)		//����������
					alpha = (vecDotL.front().alpha - M_PI + (vecDotL.back().alpha)) / 2.0;
				else				//���������
					alpha = (vecDotL.front().alpha + M_PI + (vecDotL.back().alpha)) / 2.0;
				_tmpDotFing = vecDotL.front();
				_tmpDotFing.alpha = alpha;			//����������� � �� (����������� �����)//
				_tmpDotFing.type = dAlpha<0;		//��� ��//
				historyDotL.push_front(_tmpDotFing);
				if(bestDot.alpha <= abs(dAlpha)) 
				{	
					bestDot.coord = _tmpDotFing.coord;
					bestDot.alpha = abs(dAlpha);
				}
			}
			else //������� �����//
			{
				if (!historyDotL.empty())		//��� _�������_ ������� �����
				{
					alpha = 0.0;
					for(iter = historyDotL.begin(); iter != historyDotL.end(); iter++)
						alpha += iter->alpha;
					alpha /= historyDotL.size();		//������� �������� � ���������� ��
					iter = historyDotL.begin();
					for(unsigned int i = 0; i<(historyDotL.size()/2); i++) iter++;
					CPoint wdot = bestDot.coord;		//�������� ��������� ����� ��� ��
					CPoint dotForAccept = FindAcceptDot(wdot, alpha, iter->type);
					if (dotForAccept.x != -1)
					{	//����� ����� �����������//
						COLORREF cl;
						cl = (historyDotL.begin()->type)?0x000000:0xffffff;
//����� ����� �������� � ������ �������� �����//
						pic->Line(wdot, dotForAccept, 4, cl);
						_dot->pr1 = true;	//��� ����� ��������� ��� ��� ����������������
						changeN++;
						stopKol += (stopKol-kol < 200)?200:0;
						//stopKol += (kol*1.5 > stopKol)?500:0;

						//��������� ��������� ���������
						if(!historyDotL.begin()->type)
						{	//���� ������������� �������� �� ���������� �������� ����� ����� �� �����
							_map.push_back(TMapElDot(dot));
						}
//�������� ��������� �����, ������� ������
//����� ��������, ��� ��������� ����� �� ������ �� �������
						if(ret-KOL_S*LEN_S < 0)
						{
							ret = 0;
							homeOver = false;
							stopKol = 500;
						}
						A = B = dot = resetDot.coord;
						vecDotS.clear();		
						vecDotL.clear();		
						//------------------------------
					}
					historyDotL.clear();
				}
			}
		}
		if (dot.x == _dot->coord.x && dot.y == _dot->coord.y)
		{//�������� ����� ����� ��������
			if (kol <= 2)
			{//����� ������������� ��������
				CString s;
				s.Format("%d", kol);
				if(MESSAGEOUT)MessageBox(0, "kol<=2   kol = " + s, "", MB_OK);
				return changeN;
			}else
			{
				homeOver = true;	//������ ���������� ������ ������	
				stopKol = kol + KOL_L*LEN_L*LEN_S;
			}
		}
		if (homeOver) ret++;
	}while(ret < (LEN_L*LEN_S*KOL_L) && ret < stopKol && kol <= stopKol);

	_dot->pr2 = false;
    return changeN;
}
inline double TAnalysePicture::GetAlpha(const CPoint A, const CPoint B)
//���������� �� ����� � � � [-pi,pi)
{
	if(A == B) return 0.0;
	double alpha;
	if (A.x - B.x == 0)
	{
		if (A.y > B.y) alpha = M_PI_2;
		else alpha = -M_PI_2;
	}else
	{
		double a = ((double)A.y-B.y)/((double)B.x-A.x);
		alpha = atan(a);
		if (A.x > B.x)
		{
			if (alpha < 0) alpha += M_PI;
			else alpha -= M_PI;
			if (A.y == B.y) alpha = -M_PI;
		}
	}
	return alpha;
}
bool TAnalysePicture::TestFindDot(int _x, int _y)
//���� �����: �������� ����������� ������ � ����� ������ ���� ������ 110 ��������
{
	const int len = 7;
	CPoint A(_x, _y), B, C;
//������ ������
	B = A; 
	int vec = 0;
	for(int i = 1; i<=len; i++)
		B = tmpPic->NextDotCW(B, vec);
	//------������ ����-------//
	double alpha1 = GetAlpha(A, B);
//������ ������
	C = B;
	B = A; 
	vec = 0;
	for(int i = 1; i<=len; i++)
	{
		B = tmpPic->NextDotCCW(B, vec);
		if(abs(B.x-C.x) < 3 && abs(B.y-C.y) < 3) return true;
	}
	//------������ ����-------//
	double alpha2 = GetAlpha(A, B);
//-----alpha1, alpha2------//
	alpha1 = abs(alpha2 - alpha1);
	if (alpha1 > M_PI) alpha1 = 2.0*M_PI - alpha1;
	return alpha1 < (110.0/180.0 * M_PI);
}
CPoint TAnalysePicture::FindAcceptDot(CPoint dot, double alpha, bool type)
//����� ����������� �� ���������/����������
{
	const int maxL = 11;
	const int minL = 3;
	COLORREF color;
	color = (type)?0x000000:0xffffff;
		//��������� - ���� ������ �����
		//���������� - ���� ����� �����
	int i = 0;
	while (i<=6)	//������� ������ � ��������� ����������� alpha
	{
		int l = minL;
		int k = (i+1) / 2;
		if (i % 2 == 1) k = -k;
		while (l<=maxL)
		{
			double arg = alpha + k * M_PI * 5.0/180.0;
			int x = dot.x + (int)(l*cos(arg)+0.5);
			int y = dot.y - (int)(l*sin(arg)+0.5);
			if (tmpPic->GetPixel(x, y) == color)	//������ ������� ����� �����!!!
			{
				if(TestFindDot(x,y))				//�������� ��������� ����� (�� "��������" :) )
					return CPoint(x, y);		//��������� �����
				else
					break;
			}
			l++;	//���������� ��������� ������
		}
		i++;
	}
	return CPoint(-1, -1);	//����� �� �������
}

bool TAnalysePicture::Show(int x, int y, int xt, int yt)
{
	if(xt!=-1) pic2->Show(xt, yt);
	return pic->Show(x, y);
}
TFingPicture *TAnalysePicture::GetPic1()
{
	return pic;
}
TFingPicture *TAnalysePicture::GetPic2()
{
	return pic2;
}
double TAnalysePicture::ChangeAlphaInterval(double _alpha)
//���������� ��������� � [-pi,pi)
{
	double ret = abs(_alpha);
	while(ret >= 2.0*M_PI) ret -= 2.0*M_PI;
	if(ret > M_PI) ret = 2.0*M_PI - ret;
	else ret = -ret;
	if(_alpha > 0) ret = -ret;
	return ret;
}
/*������������ ���������� �����
����������� ������������� ������������ � ��������������� ������
� ��� �� ����� ����� � ������ �� ������� ��� �����*/
int TAnalysePicture::DotsFilter(TAbsFing &_dots)
{
	int leftDots = 0;
	TAbsFing::iterator iter1;
	TAbsFing::iterator iter2;
	for(iter1 = _dots.begin(); iter1 != _dots.end(); iter1++)
	{
		if(!iter1->show) continue;
		//����� ����� ������� �������� (������� ���������)
		iter1->show = LeftDot(iter1);
	}
	for(iter1 = _dots.begin(); iter1 != _dots.end(); iter1++)
	{
		if(!iter1->show) continue;

		//����� ������������� �����
		for(iter2 = iter1, ++iter2; iter2 != _dots.end(); iter2++)
		{
			if(!iter2->show) continue;
			double difL = GetS(iter1->coord,iter2->coord);
			if( //������� ������
				(
				//�� ������� ��������� (15) ��������� ��� ���������/���������� ������������ ���� �� �����
					(difL < 15)&&
					((abs(iter2->alpha - iter1->alpha) > (165.0/180.0*M_PI))&&(abs(iter2->alpha - iter1->alpha)<(195.0/180.0*M_PI)))
				)
				||
				(	
				//��� ������ ����� ������� ����� (<5..10) 
					(difL < 10)&&(iter1->type == iter2->type)
				)
			)
			{
				iter1->show = false;
				iter2->show = false;
			}
		}
	}
	return leftDots;
}
inline double TAnalysePicture::GetS(CPoint A, CPoint B)
//��������� ����� �������
{
	return sqrt( (double)((A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y)) );
}
/*���� ����� �������� ����������, �� ����� � ������ �� ��� ������ ���� �����
���� ��� �� ���, �� ����� ����� ��������� �� ����������� �������*/
bool TAnalysePicture::LeftDot(TAbsFing::iterator &iter)
{
	COLORREF color = 0x000000;				//���� ������ ����� ��� ���������
	if(!iter->type) color = 0xffffff;;		//���� ����� ����� ��� ����������
	
	int l, k = 35;
	const int minL = 4, maxL = 12;
	bool find = false;
	while(k <= 55)
	{
		l = minL;
		while(l <= maxL)
		{
			int x = iter->coord.x + (int)(l*cos(iter->alpha + k/180.0*M_PI)+0.5);
			int y = iter->coord.y - (int)(l*sin(iter->alpha + k/180.0*M_PI)+0.5);
			if(pic->GetPixel(x,y) == color)	// ������ �������!!!
			{	find = true; break;}		//����� ����� �����
			l++;
		}
		if(find) break;
		k += 10;	//����� � ����� 10��
	}
	if(!find) return false;
	k = 35;
	while(k <= 55)
	{
		l= minL; 
		while(l <= maxL)
		{
			int x = iter->coord.x + (int)(l*cos(iter->alpha - k/180.0*M_PI)+0.5);
			int y = iter->coord.y - (int)(l*sin(iter->alpha - k/180.0*M_PI)+0.5);
			if(pic->GetPixel(x,y) == color) // ������ �������!!!
				return true;				//����� ����� ������
			l++;
		}
		k += 10;
	}
	return false;
}
