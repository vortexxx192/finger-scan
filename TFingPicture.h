#pragma once
#include "fing.h"

///////////////////////////////////////////////////////////////////////////////
//����� �����������.
//�������� �����������, ���������� ���������� �������� ��� ���
///////////////////////////////////////////////////////////////////////////////
class TFingPicture
{
private:
	CDC pic;			//��������� �� �����������
	BITMAP bmp;			//�����������
	bool IsLoad;		//����������� ���������
	CDC *Screen;		//��������� �� ���� ���������
public:
	TFingPicture(CDC *_Screen);		//_Screen - ��������� �� ����
	~TFingPicture(void);
	bool Load(const CString src);		//��������� ����������� �� ����� src
	bool Show(int X, int Y);			//���������� ����������� �� ���� � ����������� (X,Y)
	bool SetPixel(CPoint dot, COLORREF color);		//��������� ����� ������� dot
	bool SetPixel(int x, int y, COLORREF color);	//��������� ����� ������� (x,y)
	COLORREF GetPixel(CPoint dot);					//������ ����� ������� dot
	COLORREF GetPixel(int x, int y);				//������ ����� ������� (x,y)
	bool FloodFill(CPoint dot, COLORREF color=0xffffff);	//������� ������� (�� ��������� ������ ������)
	bool FloodFill(int x, int y, COLORREF color=0xffffff);	//������� ������� (�� ��������� ������ ������)
	bool Line(CPoint from, CPoint to, int width, COLORREF color);	//��������� �����
	bool Rectangle(CPoint from, CPoint to, int width=2, COLORREF color=0xffffff);	//��������� ��������������
	bool Copy(TFingPicture &from);			//����������� �����������
	CPoint NextDotCW(const CPoint dot, int &vec);		//����� ��������� ����� "_�� �������_ �������"
	CPoint NextDotCCW(const CPoint dot, int &vec);		//����� ��������� ����� "_������ �������_ �������"
	CPoint GetSize();					//��������� ������� �����������
};
