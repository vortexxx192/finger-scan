//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FingerAnalyse`r.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FINGERANALYSER_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDR_TOOLBAR                     130
#define IDI_FING_ICON                   135
#define IDR_MENU1                       138
#define IDC_OPEN_FILE                   1000
#define IDC_ANALYSE                     1001
#define IDC_COMPARE                     1002
#define IDC_EXIT                        1003
#define IDC_SAVE_TO_DB                  1004
#define IDC_SPEC_DOT                    1005
#define IDC_LOAD_PROGRESS               1006
#define IDC_WORK_FILE                   1007
#define IDC_LOAD_COMPARE_PROGRESS       1008
#define IDC_TEMESCAN                    1009
#define IDC_BUTTON_PREV                 1012
#define IDC_BUTTON_NEXT                 1013
#define IDC_SHOW_BASE                   1014
#define IDC_EDIT1                       1015
#define ID_BASE                         32771
#define ID_PROPERTY                     32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
