#pragma optimize("", off)
#include "fing.h"
#include <atlstr.h>
#include <atltypes.h>
#define _USE_MATH_DEFINES
#include <cmath>

using namespace std;


bool TAbsFing::SaveFing(CString fsav)
//���������� ��������� � ���� *.sav
{
	if(!this->size()) return false;
	TAbsFing::iterator iter;

	FILE *fingfile = fopen(fsav, "wb"); 
	if(fingfile == NULL)
	{
		MessageBox(NULL,"���������� ������� ����: '"+fsav+"'", "������ ������ � ������", MB_OK);
		return false;
	}
	for(iter = this->begin(); iter != this->end(); iter++)
	{
		TAbsDot dot = *iter;
		if(iter->show) fwrite((void *)&dot, 1, sizeof(dot), fingfile);
	}
	fclose(fingfile);
	return true;
}
bool TAbsFing::LoadFing(CString src)
//�������� ��������� �� ����� *.sav
{
	TAbsDot dot;

	FILE *fingfile = fopen(src, "rb");
	if(fingfile == NULL)
	{
		MessageBox(NULL,"���������� ������� ����: '"+src+"'", "������ ������ � ������", MB_OK);
		return false;
	}
	this->clear();
	while(!feof(fingfile))
	{
		fread((void *)&dot, 1, sizeof(dot), fingfile);
		this->push_back(dot);
	}
	this->pop_back();
	fclose(fingfile);
	return true;
}
///////////////////////////////////////////////////////////////////////////////////////
///TRelFing//TRelFing/TRelFing/TRelFing/TRelFing/TRelFing/TRelFing/TRelFing/TRelFing///
///////////////////////////////////////////////////////////////////////////////////////
TRelFing *TRelFing::Convert(TAbsFing &fng)
//�������������� ���������� ��������� � �������������
{
	if(fng.empty()) return this;
	this->clear();
	TAbsFing::iterator iterA1, iterA2;
	TRelDot tmpR;
	listTRelDot listDots;
	double tmpa, vecAB;
	for(iterA1 = fng.begin(); iterA1 != fng.end(); iterA1++)
	{	
		for(iterA2 = fng.begin(); iterA2 != fng.end(); iterA2++)
		{
			if(iterA2 == iterA1) continue;
			tmpR.l = (short)(GetS(iterA1->coord, iterA2->coord)+0.5);	//l - ��������� ����� �������
			vecAB = GetAlpha(iterA2->coord, iterA1->coord);
			tmpa = iterA1->alpha - vecAB;
			if(tmpa < 0) tmpa = 2*M_PI + tmpa;
			tmpR.a1 = (short)(tmpa * 180.0/M_PI +0.5);	//a1 - ���� ����� ����������� ������������ ����� � � ������������ A -> B
			tmpa = iterA2->alpha - vecAB;
			if(tmpa < 0) tmpa = 2*M_PI + tmpa;
			tmpR.a2 = (short)(tmpa * 180.0/M_PI +0.5);	//a2 - ���� ����� ����������� ������������ ����� � � ������������ A -> B
			tmpR.absDot = *iterA1;			//�� ���� ������ �������� ���� � �� �� ������!(���������� ��� ����������� ��������� �����)
			listDots.push_back(tmpR);
		}
		listDots.sort();
		this->push_back(listDots);
		listDots.clear();
	}
	return this;
}
inline double TRelFing::GetS(const CPoint A, const CPoint B)
//��������� ����� �������
{
	return sqrt( (double)((A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y)) );
}
double TRelFing::GetAlpha(const CPoint A, const CPoint B)
//���������� �� ����� � � � [-pi,pi)
{
	if(A == B) return 0.0;
	double alpha;
	if (A.x - B.x == 0)
	{
		if (A.y > B.y) alpha = M_PI_2;
		else alpha = -M_PI_2;
	}else
	{
		double a = ((double)A.y-B.y)/((double)B.x-A.x);
		alpha = atan(a);
		if (A.x > B.x)
		{
			if (alpha < 0) alpha += M_PI;
			else alpha -= M_PI;
			if (A.y == B.y) alpha = -M_PI;
		}
	}
	return alpha;
}
TCompareFing TRelFing::Compare(TRelFing &fng)
//�������� ��������� � ���������� �� �����
{
	TCompareFing ret;
	ret.nfng = (short)fng.size();
	const short CONFIRM_VAL = 9;
	const double DELTA_L = 10.0;			//������������
	const double DELTA_A = 10.0;			//������������

	short confirmDot = 0;		//���������� ��������� �� (���� �����)
	short confirmVal = 0;		//���������� ��������� ����������� �� � ������� ��
	short needVal = (short)(min(this->size(),fng.size())/3.0 +0.5);
    if(needVal > CONFIRM_VAL) needVal = CONFIRM_VAL;

	listTRelDot *surroundDots1, *surroundDots2;
	listTRelDot::iterator baseIter;
	for(TRelFing::iterator tekFing = this->begin();
		tekFing != this->end();
		tekFing++)
	{
		for(TRelFing::iterator baseFing = fng.begin();
			baseFing != fng.end();
			baseFing++)
		{
			confirmVal = 0;
			surroundDots1 = new(listTRelDot);
			surroundDots2 = new(listTRelDot);
			for(listTRelDot::iterator tekIter = (*tekFing).begin();
				tekIter != (*tekFing).end();
				tekIter++)
			{
				baseIter = (*baseFing).begin();
				short prev, next;
				prev = next = abs(baseIter->l - tekIter->l);
				while(
					prev >= next &&
					next >= DELTA_L && 
					baseIter != (*baseFing).end()) 
				{	
					prev = next;
                    baseIter++;
					next = abs(baseIter->l - tekIter->l);
				}
				if(prev >= DELTA_L && prev < next) continue;	//��� ������ ���������� ������ �.�. ������ ����� next >= DELTA_L
				for(;
					baseIter != (*baseFing).end();
					baseIter++)
				{
					int len = abs(tekIter->l - baseIter->l);
					if(len >= DELTA_L) break;		//��� ������ ���������� ������ �.�. ������ ����� next >= DELTA_L
					int delta_a = DELTA_A;
					if(
						((abs(tekIter->a1 - baseIter->a1)<delta_a)||(abs(tekIter->a1 - baseIter->a1) > 360-delta_a))&&
						((abs(tekIter->a2 - baseIter->a2)<delta_a)||(abs(tekIter->a2 - baseIter->a2) > 360-delta_a)))
					{
						confirmVal++;

						surroundDots1->push_back(*baseIter);
						surroundDots2->push_back(*tekIter);

						break;
					}
				}
				if(confirmVal > needVal)
				{	
					///////////////////////////
					//������ ��� ����� �� ������������ ��������, �.�. ��� ��� �������
					ret.dots.push_back(TPairAbsDot(baseFing->back().absDot, tekFing->back().absDot));
					ret.surdots.push_back(TPairSur(surroundDots1,surroundDots2));
					baseFing->clear();
					fng.erase(baseFing);
					confirmDot++;
					break;
				}
			}
            if(confirmVal > needVal){break;}
			else{
 				ret.dots.push_back(TPairAbsDot(baseFing->back().absDot, tekFing->back().absDot));
				ret.surdots.push_back(TPairSur(surroundDots1,surroundDots2));

				surroundDots1->clear();
				surroundDots2->clear();
			}
		}
	}
	ret.cDot = confirmDot;
	ret.val = 0;
	return ret;
}
