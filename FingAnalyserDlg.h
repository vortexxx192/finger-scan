// FingerAnalyserDlg.h : header file
//
#pragma once
#include "TFingPicture.h"
#include "afxcmn.h"
typedef list<TInfo> listTInfo;
// CFingerAnalyserDlg dialog
class CFingerAnalyserDlg : public CDialog
{
// Construction
public:
	CFingerAnalyserDlg(CWnd* pParent = NULL);	// standard constructor
	~CFingerAnalyserDlg();	// ����������
// Dialog Data
	enum { IDD = IDD_FINGERANALYSER_DIALOG };
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
// Implementation
protected:
	HICON m_hIcon;
	CDC memDC;
	CBitmap bm;
	BITMAP bmp;
	UINT timer;
	TFingPicture *fp;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOpenFile();
	afx_msg void OnBnClickedExit();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedAnalyse();
	afx_msg void OnBnClickedCompare();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnEnChangeSpecDot();
	int m_kolDots;
	afx_msg void OnBnClickedSaveToDb();
	CProgressCtrl loadProgress;
public:
	listTInfo *LoadDB(CString dbFile);
	list<TCompareFing> *CompareWithBase();
	CString m_workFile;
	CProgressCtrl compare_progress;
	long m_scantime;
	afx_msg void OnBnClickedButtonPrev();

	list<TCompareFing> *compareResult;
	list<TCompareFing>::iterator showIter;
	//list<TCompareFing>::iterator surdots;
	afx_msg void OnBnClickedButtonNext();
	void ShowBase(bool key, bool next = true);
	void PrintReport(CString file, CString report);
	CString GetSAV(CString srcName);		//��������� ���� � sav �����
	BOOL m_show_base;
	afx_msg void OnBnClickedShowBase();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	CPoint mouse_pos;
	int m_mouse_x;
	int m_mouse_y;
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
