// FingerAnalyserDlg.cpp : implementation file
//
#pragma optimize("", off)
#define _CRT_SECURE_NO_DEPRECATE
#include "FingAnalyser.h"
#include "FingAnalyserDlg.h"
#include "TAnalysePicture.h"
#include <atlstr.h>
#include <atltypes.h>
#include <afxdlgs.h>
#include <string>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CString sav_path, db_file;
TAnalysePicture *picture;
TAbsFing fingA;
TRelFing fingR;

// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{}
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()
// CFingerAnalyserDlg dialog
CFingerAnalyserDlg::CFingerAnalyserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFingerAnalyserDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// ������������� ����������
	m_workFile = _T("");			// ������� ����
	m_show_base = FALSE;				// �������� ��������
	m_mouse_x = 0;						// ���������� �����
	m_mouse_y = 0;
	m_kolDots = 0;					// ���������� �����
	m_scantime = 0;					// ����� �������
	picture = NULL;
}
CFingerAnalyserDlg::~CFingerAnalyserDlg()
{
	delete(fp);
	delete(picture);
	if(compareResult) 
	{	
		for(list<TCompareFing>::iterator i = compareResult->begin();
			i != compareResult->end();
			i++)
		{	
			list<TPairSur>::iterator j;
			for(j=i->surdots.begin(); j!=i->surdots.end(); j++)
			{
				j->first->clear();  delete(j->first);
				j->second->clear(); delete(j->second);
			}
		}
		compareResult->clear();
		delete(compareResult);
		compareResult = NULL;

		// ��������� 12.06.17 10:27
		compareResult = CompareWithBase();
		showIter = compareResult->begin();

	}
}

void CFingerAnalyserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SPEC_DOT, m_kolDots);
	DDX_Control(pDX, IDC_LOAD_PROGRESS, loadProgress);
	DDX_Text(pDX, IDC_WORK_FILE, m_workFile);
	DDX_Control(pDX, IDC_LOAD_COMPARE_PROGRESS, compare_progress);
	DDX_Text(pDX, IDC_TEMESCAN, m_scantime);
	DDX_Check(pDX, IDC_SHOW_BASE, m_show_base);
}

BEGIN_MESSAGE_MAP(CFingerAnalyserDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_OPEN_FILE, OnBnClickedOpenFile)
	ON_BN_CLICKED(IDC_EXIT, OnBnClickedExit)
	ON_WM_CLOSE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_ANALYSE, OnBnClickedAnalyse)
	ON_BN_CLICKED(IDC_COMPARE, OnBnClickedCompare)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SAVE_TO_DB, OnBnClickedSaveToDb)
	ON_BN_CLICKED(IDC_BUTTON_PREV, OnBnClickedButtonPrev)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_SHOW_BASE, OnBnClickedShowBase)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CFingerAnalyserDlg message handlers

BOOL CFingerAnalyserDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	fp = new TFingPicture(this->GetDC());

	char fullpath[200];
	_fullpath(fullpath, NULL, 200);
	sav_path = fullpath;
	sav_path += "\\sav\\";
	db_file = sav_path + "fingbase.bse";

	compareResult = NULL;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFingerAnalyserDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFingerAnalyserDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if(m_show_base)
	{//����� ��������� ����
		ShowBase(true, false);
	}
	else
	{//����� ��������� ��������� ������
		if (picture != NULL)
		{
			picture->GetPic1()->Show(110, 45);
			picture->GetPic2()->Show(545, 45);
		}
		m_kolDots = (int)fingA.size();
		UpdateData(false);
	}

	CDialog::OnPaint();
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFingerAnalyserDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFingerAnalyserDlg::OnBnClickedOpenFile()
{
	char szFilters[]= "������ (*.bmp)|*.bmp|All Files (*.*)|*.*||";
	CFileDialog dlg(TRUE, "bmp", "*.bmp", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, szFilters, this);

	if(dlg.DoModal() != IDOK) return;		//������� ������ �� �������
	if(dlg.GetFileExt().CompareNoCase("bmp")) return;	//�������� ���� �� ����� ��������� .bmp

//	fp->Load(dlg.GetFileName());
	CString fileName = dlg.GetFileName();
	delete(picture);
	picture = new TAnalysePicture(fileName, this->GetDC());
	m_workFile = fileName;

	if(compareResult)
	{
		for(list<TCompareFing>::iterator i = compareResult->begin();
			i != compareResult->end();
			i++)
		{	
			list<TPairSur>::iterator j;
			for(j=i->surdots.begin(); j!=i->surdots.end(); j++)
			{
				j->first->clear(); delete(j->first);
				j->second->clear(); delete(j->second);
		}	}
		compareResult->clear();
	}
	m_show_base = false;
	Invalidate();
}

void CFingerAnalyserDlg::OnBnClickedExit()
{
	CDialog::SendMessage(WM_CLOSE);
}
void CFingerAnalyserDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

//	pWndOther->SetProperty(
}

void CFingerAnalyserDlg::OnBnClickedAnalyse()
{
	if(picture == NULL) return;

	LPSYSTEMTIME mTime;
	mTime = new SYSTEMTIME;
	GetSystemTime(mTime);
	long int workTime;
	workTime = mTime->wSecond*1000+mTime->wMilliseconds;

		fingA = picture->AnalysePicture();
		fingA.SaveFing(GetSAV(picture->getPathSrc()));
	
	GetSystemTime(mTime);
	workTime = mTime->wSecond*1000+mTime->wMilliseconds - workTime;
	workTime = (workTime<0)?60000+workTime:workTime;
	delete(mTime);

    m_scantime = workTime;

	Invalidate();
}

void CFingerAnalyserDlg::OnBnClickedCompare()
{
	if(fingA.size() == 0) 
	{	
		MessageBox("��������� �� ���������", "������");
		return;
	}
	fingR.Convert(fingA);
	if(compareResult) 
	{	
		for(list<TCompareFing>::iterator i = compareResult->begin();
			i != compareResult->end();
			i++)
		{	
			list<TPairSur>::iterator j;
			for(j=i->surdots.begin(); j!=i->surdots.end(); j++)
			{
				j->first->clear(); delete(j->first);
				j->second->clear(); delete(j->second);
			}
		}
		compareResult->clear();
		delete(compareResult);
		compareResult = NULL;
		//showIter = NULL; �����������, �.�. ����� � �����, ��� ��� ����� �� ���������, �� ������
	}
	compareResult = CompareWithBase();
	if(compareResult->size() == 0) return;
	CString sOut="";
	for(list<TCompareFing>::iterator i = compareResult->begin();
		i != compareResult->end();
		i++)
	{
		CString s="";
		int mlevel = min(i->nfng,m_kolDots);
		int percent;
		if(mlevel > 10) mlevel = 10;
		if(i->cDot > mlevel) percent = 100;
		else percent = (int)(100.0*i->cDot/(double)mlevel + 0.5);
		if(percent == 0) continue;
		s.Format("%d %d %% %s\n", i->cDot, percent, i->name);
		sOut += s;
	}
	if(sOut.GetLength()==0) sOut = "�� ������ ��������� �� �������!\n";
	CString kol; kol.Format("\n ����� � ����: %d", compareResult->size());
	sOut += kol;
	PrintReport(picture->getPathSrc(), sOut);
	MessageBox(sOut, picture->getPathSrc()); 
}

void CFingerAnalyserDlg::OnTimer(UINT nIDEvent)
{
	Invalidate();
	CDialog::OnTimer(nIDEvent);
}
void CFingerAnalyserDlg::OnBnClickedSaveToDb()
{
	char szFilters[]=
      "������ (*.bmp)|*.bmp|All Files (*.*)|*.*||";
	CFileDialog dlg(TRUE, "bmp", "*.bmp", OFN_FILEMUSTEXIST| OFN_HIDEREADONLY| OFN_ALLOWMULTISELECT, szFilters, this);
	if(dlg.DoModal() == IDOK)
	{
		listTInfo *fingDB = LoadDB(db_file);
	
		FILE *fbse = fopen(db_file, "wb");
		if(fbse == NULL)
		{
			MessageBox("���������� ������� ���� ������ � �����������", "������ �������� ��", MB_OK);
			return;
		}
		
		POSITION pos, posStart;
		TInfo newFingInDB;
		pos = posStart = dlg.GetStartPosition();
		int kolFile = 0;
		while(pos) {dlg.GetNextPathName(pos); kolFile++;}
		pos = posStart;
		loadProgress.SetRange(0, kolFile);
		int progressPos = 1;
		while(pos)
		{
			CString fileName = dlg.GetNextPathName(pos).MakeLower();
			if(fileName.Find(".bmp") == -1) continue;
			TAnalysePicture *loadingPic;
			loadingPic = new TAnalysePicture(fileName, this->GetDC());
			m_workFile = fileName;
			fingA = loadingPic->AnalysePicture();
			//if(fingA.size() < MIN_SIZE) {MessageBox("��������� �� �������� ��� ����������� � ����!", fileName); continue;}
			//if(fingA.size() > MAX_SIZE) {MessageBox("��������� �� �������� ��� ����������� � ����!", fileName); continue;}

			fingA.SaveFing(GetSAV(fileName));
			newFingInDB.src = fileName;
			fingDB->remove(newFingInDB);
			fingDB->push_back(newFingInDB);
			loadProgress.SetPos(progressPos);
			progressPos++;
			Invalidate();
			delete(loadingPic);
		} 
		loadProgress.SetPos(0);

		int count = 0;
		fwrite((void*)&count, sizeof(count), 1, fbse);
		for(list<TInfo>::iterator iter = fingDB->begin(); iter != fingDB->end(); iter++)
		{
			iter->Printf(fbse);
			count++;
		}
		fseek(fbse, 0, SEEK_SET);
		fwrite((void*)&count, sizeof(count), 1, fbse);
		fingDB->clear();
		delete(fingDB);
		fclose(fbse);
	}
}
listTInfo *CFingerAnalyserDlg::LoadDB(CString dbFile)
//��������� ����� �� ��
{
	listTInfo *bse = new listTInfo();
	TInfo finf;			//������ �� ���������
	FILE *fbse = fopen(dbFile, "rb");
	if(fbse == NULL)
	{
//		MessageBox("���������� ��������� ���� ������ � �����������", "������ �������� ��", MB_OK);
		return bse;
	}

	int count = 0;
	fread((void*)&count, sizeof(count), 1, fbse);
	for(;count > 0; count--)
	{
		finf.Scanf(fbse);
		bse->push_back(finf);
	}
	fclose(fbse);
	return bse;
}
list<TCompareFing> *CFingerAnalyserDlg::CompareWithBase()
//�������� ����� � ������� � ��
{
	listTInfo *bse;
	list<TCompareFing> *cFng;
	cFng = new list<TCompareFing>;
	bse = LoadDB(db_file);
	if(bse->empty()) 
	{	
		MessageBox("���� ������ ���������� �����", "���������", MB_OK);
		return cFng;
	}
	TAbsFing aFng;
	TRelFing baseFng;
	compare_progress.SetRange(0, (short)bse->size());
	for(list<TInfo>::iterator ibse = bse->begin();
		ibse != bse->end(); ibse++)
	{
		if(!aFng.LoadFing(GetSAV(ibse->src))) continue;
		baseFng.Convert(aFng);
		TCompareFing compareRes = fingR.Compare(baseFng);
		compareRes.name = ibse->src;
		cFng->push_back(compareRes);
		compare_progress.SetPos((int)cFng->size());
	}
	bse->clear();
	compare_progress.SetPos(0);
	delete(bse);
	return cFng;
}

void CFingerAnalyserDlg::OnBnClickedButtonPrev(){	ShowBase(false);}
void CFingerAnalyserDlg::OnBnClickedButtonNext(){	ShowBase(true);}
void CFingerAnalyserDlg::ShowBase(bool key, bool next)
//key - ����������� ��������� �� ���� (�����, ������)
//next - ����� �� ���������� � ���������� ���������
{
	if(!compareResult) return;
	if(compareResult->size() == 0)
	{	
		MessageBox("���� ������ ���������� �����", "���������", MB_OK);
		return;
	}
	//if(showIter == NULL) *showIter = compareResult->begin();
	else 
	{	
		if(next)
			if(key) 
			{
				showIter++;
				if(showIter == compareResult->end())
					showIter = compareResult->begin();
			}
			else
			{
				if(showIter == compareResult->begin()) 
					showIter = compareResult->end();
				showIter--;
			}
	}

	TFingPicture *pic;
	pic = new TFingPicture(this->GetDC());
	if(!pic->Load(BLANK)) return;
			
	CPaintDC dc(this); // device context for painting
	
	list<TPairSur>::iterator is = showIter->surdots.begin();
	list<TPairAbsDot>::iterator id = showIter->dots.begin();
	for(; id != showIter->dots.end(); id++, is++)
	{	
		COLORREF col;
		if(is->first->empty())	col = 0xBBBBBB;
		else					col = (id->first.type)?0xff0000:0x000000;
		pic->Line(id->first.coord, id->first.coord, 5, col);
		pic->Line(id->first.coord, 
					CPoint(id->first.coord.x+(int)(10.0*cos(id->first.alpha)),id->first.coord.y-(int)(10.0*sin(id->first.alpha))),
					2, col);
		if(is->first->empty()) continue;	//��������� ��� ���� ����� ���
		//��������, ��� "����" ��������� ��� ������
		if( abs(mouse_pos.x-id->first.coord.x)<6 && abs(mouse_pos.y-id->first.coord.y)<6 )
		{
			TFingPicture pic2(this->GetDC());
			if(!pic2.Load(BLANK)) return;
			pic2.Copy(*picture->GetPic2());
			for(listTRelDot::iterator ii = is->first->begin(); ii != is->first->end(); ii++)
			{	
				COLORREF cl = 0x554444;
				CPoint cd;
				cd.x = (long)(id->first.coord.x - ii->l * cos(ii->a1*M_PI/180.0 - id->first.alpha));
				cd.y = (long)(id->first.coord.y - ii->l * sin(ii->a1*M_PI/180.0 - id->first.alpha));
				pic->Line(id->first.coord, cd, 1, cl);
			}
			for(listTRelDot::iterator ii = is->second->begin(); ii != is->second->end(); ii++)
			{	
				COLORREF cl = 0x554444;
				CPoint cd;
				cd.x = (long)(id->second.coord.x - ii->l * cos(ii->a1*M_PI/180.0 - id->second.alpha));
				cd.y = (long)(id->second.coord.y - ii->l * sin(ii->a1*M_PI/180.0 - id->second.alpha));
				pic2.Line(id->second.coord, cd, 1, cl);
			}
			pic2.Show(545, 45);
		}
 	}

	if (pic != NULL)
	{
        pic->Show(110, 45);
		m_workFile = showIter->name;
	}
	UpdateData(false);
	delete(pic);
}
void CFingerAnalyserDlg::PrintReport(CString file, CString report)
{
	FILE *outf = fopen("report.txt", "a");
	CString msg = "\n------ "+file+" ------\n"+report;
	fprintf(outf, msg);
	fclose(outf);
}
CString CFingerAnalyserDlg::GetSAV(CString srcName)
{
	CString fsav = srcName.Left(srcName.GetLength() - 3) + "sav";
	while(fsav.Find("\\") != -1){ fsav = fsav.Right(fsav.GetLength() - fsav.Find("\\")-1); }
	return sav_path + fsav;
}
void CFingerAnalyserDlg::OnBnClickedShowBase()
{
	m_show_base =! m_show_base;
	UpdateData(false);
	if(m_show_base)
	{
		ShowBase(true, false);
	}
	else
	{
		OnPaint();
	}
}
// ��������� 12.06.2016 10:43
void CFingerAnalyserDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CDialog::OnMouseMove(nFlags, point);
}
void CFingerAnalyserDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(!m_show_base) return;
	mouse_pos = point;
	mouse_pos.x -= 110;
	mouse_pos.y -= 45;
	ShowBase(true, false);

	CDialog::OnLButtonDown(nFlags, point);
}