// FingerAnalyser.h : main header file for the PROJECT_NAME application
//
#pragma once
#ifndef __AFXWIN_H__
#endif
#include "resource.h"		// main symbols
#include <afxwin.h>
// CFingerAnalyserApp:
// See FingerAnalyser.cpp for the implementation of this class
//
class CFingerAnalyserApp : public CWinApp
{
public:
	CFingerAnalyserApp();
// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation
	DECLARE_MESSAGE_MAP()
};
extern CFingerAnalyserApp theApp;
