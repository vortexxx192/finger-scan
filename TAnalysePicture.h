#pragma once
#include "TFingPicture.h"

//MESSAGEOUT ���������� ���������� ���������� � ������� popup ����
//#define MESSAGEOUT true
#define MESSAGEOUT true
#define OUT_FILE "fingAnalyserOut.txt"	//���� �����
#define BLANK "blank.bmp"				//������ �����������

///////////////////////////////////////////////////////////////////////////////////
//������ ��������� ��� ������ �����������
#define LEN_S 3				//����� ������ ������� (LEN_S �����)
#define LEN_L 4				//����� �������� ������� (LEN_L ����� ��������)
#define KOL_L 2				//����������� ���������� ������� ��������
#define KOL_S LEN_L*KOL_L	//����������� ���������� �����
#define TEST_ALPHA 130.0	//���� �� �������� �������. ����������� ���� � ��������
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
// ����� ������� �����������
///////////////////////////////////////////////////////////////////////////////////
class TAnalysePicture
{
private:
	TFingPicture *pic;		//���������� ���� ��������
	TFingPicture *tmpPic;	//����� ��������
	TFingPicture *pic2;		//����������� ��� ����������� � ����
	int height, width;	//������ � ������ �����������
	CString srcImg;		//���� � �����������
	int err;			//��� ��������� ��������
	TInfo info;			//���������������� ����������

private:
	int ChangeLine(list<TMapElDot>::iterator _dot, list<TMapElDot> &_map);	//��������� ��������, �� ���������
	TAbsFing ReadPic(list<TMapElDot>::iterator _dot);			//���������� �� ����������� ���� �����
	list<TMapElDot> LookPic();									//������������ �������� � ���������� ����� �� ���
	inline double GetAlpha(const CPoint A, const CPoint B);		//���������� �� ����� � � � [-pi,pi)
	inline double GetS(CPoint A, CPoint B);						//��������� ����� �������
	CPoint FindAcceptDot(CPoint dot, double alpha, bool type);	//����� ����������� �� ���������/����������
	bool TestFindDot(int _x, int _y);							//���� �����: �������� ����������� ������ � ����� ������ ���� ������ 110 ��������
	double ChangeAlphaInterval(double _alpha);		//���������� ��������� � [-pi,pi)
	int DotsFilter(TAbsFing &_dots);	
/*������������ ���������� ����� ����������� ������������� ������������ � ��������������� ������
� ��� �� ����� ����� � ������ �� ������� ��� �����*/
	bool LeftDot(TAbsFing::iterator &iter);	
/*���� ����� �������� ����������, �� ����� � ������ �� ��� ������ ���� �����
���� ��� �� ���, �� ����� ����� ��������� �� ����������� �������*/

public:
	TAnalysePicture(const CString src, CDC *screen);
	~TAnalysePicture(void);
	int getErr();
	CString getErrMsg();
	CString getPathSrc(){return srcImg;};
	TAbsFing AnalysePicture();							//��������� ������������ ����������� � ��������� ������
	bool Show(int x, int y, int xt=-1, int yt=-1);
	TFingPicture *GetPic1();
	TFingPicture *GetPic2();
};
